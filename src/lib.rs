#![no_std] // don't link the Rust standard library
#![feature(abi_x86_interrupt)] // to allow the use because of instability of this resource
#![feature(asm)]
#![feature(try_from)]


extern crate bootloader_precompiled;
extern crate spin;
extern crate volatile;
#[macro_use]
extern crate lazy_static;
extern crate uart_16550;
extern crate x86_64;
extern crate pic8259_simple;

#[cfg(test)]
extern crate array_init;
#[cfg(test)] // allow the standard library if running tests
extern crate std;

// We need to make these public
#[macro_use]
pub mod vga_buffer;
pub mod gdt;
pub mod serial;
pub mod interrupts;
pub mod power;
pub mod math;
pub mod prmt;

// hlt is an instruction that puts the cpu into a sleep state until the
// next interrupt happens
pub fn hlt_loop() -> ! {
    loop {
        x86_64::instructions::hlt();
    }
}
