pub extern fn math(args: [u8; 75]){
    let mut first_num = 0.0;
    let mut second_num = 0.0;
    let mut result = 0.0;
    let mut next = false;
    let mut op = 0;
    let mut valid = true;

    for i in args.iter() {
        if *i != 0{
            if check_numb(*i) {
                if !next {
                    first_num = first_num * 10.0 + ((i - 48) as f64);
                }
                else {
                    second_num = second_num * 10.0 + ((i - 48) as f64);
                }
            }
            else if is_operator(*i){
                op = *i;
                next = true;
            }
        }
        else{
            break;
        }
    }

    if op == b'+' {
        result = first_num + second_num;
    }
    else if op == b'-'{
         result = first_num - second_num;
    }
    else if op == b'*' {
        result = first_num * second_num;
    }
    else if op == b'/' {
        if second_num == 0.0 {
            valid = false;
        }
        else{
            result = first_num / second_num;
        }
    }
    else {
        valid = false;
    }


    if valid {
        println!("{}", result);
    }
    else{
        println!("invalid format! run HELP MATH for more info");
    }
}

pub fn check_numb(i:u8) -> bool{
    if (i == b'0')||(i == b'1')||(i == b'2')||(i == b'3')||(i == b'4')||(i == b'5')||(i == b'6')||(i == b'7')||(i == b'8')||(i == b'9')||(i == b'0')  {
        return true;
    }
    else {
        return false;
    }
}
fn is_operator(i:u8) -> bool{
    if (i == b'+')||(i == b'-')||(i == b'*')||(i == b'/')||(i == b'%') {
        return true;
    }
    else {
        return false;
    }
}
