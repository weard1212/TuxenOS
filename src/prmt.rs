use core::str;
//read the u8 array as characters onto the screen.
pub fn u8_array_to_char(args: [u8; 80]) {
    println!();
    let length = args.iter().position(|&r| r == 0).unwrap_or(80);

    //get the position of the first space if it exists.
    let first_space = args.iter().position(|&r| r == b' ').unwrap_or(100);

    let command;
    if length < first_space {
        command = length;
    }
    else {
        command = first_space;
    }

    //parameters passed are added to this array
    let mut parameters: [u8; 75] = [0; 75];
    let mut params = false;

    if command + 3 <= length {

        for i in 0..75 {
            if (first_space + 1 + i) == 80{
                break;
            }
            parameters[i] = args[first_space + 1 + i];
            if args [first_space + i] == 0 {
                break;
            }
        }
        params = true;
    }


    if command == 4 {
        if b'H' == args[0] && b'E' == args[1] && b'L' == args[2] && b'P' == args[3] {

            if !params {
                println!("The following commands are available: ");
                println!("HELP      --used to find out more about commands", );
                println!("SHUTDOWN  --Used to power down the system.");
                println!("CLEAR     --Used to clear all text on the screen.");
                println!("MATH      --Used to do simple math calculations.");
                println!("COLOR     --Changes the color of the text.");
                println!("COPY      --Copy the text into the prompt.");
                println!("NEOFETCH  --Fetches system information.");
                println!("COWSAY    --Say something in cow.");
                println!("To learn more about a command. Type: HELP <COMMAND>");
            }
            else {
                if b'H' == parameters[0] && b'E' == parameters[1] && b'L' == parameters[2] && b'P' == parameters[3] {
                    println!("COMMAND; HELP [command]");
                    println!();
                    println!("This is a command to be used to get more information about commands");
                    println!("It will show if the command can take paramaters and what those are.");
                    println!();
                    println!("PARAMETERS:");
                    println!("[command] any command listed when calling HELP can be passed to learn more.");

                }
                else if  b'M' == parameters[0] && b'A' == parameters[1] && b'T' == parameters[2] && b'H' == parameters[3] {
                    println!("COMMAND; Math [EQUATION]");
                    println!();
                    println!("This is a command to do simple one operator math");
                    println!();
                    println!();
                    println!("PARAMETERS:");
                    println!("MATH <number><operator><number> valid operators are +, -, /, and *");
                    println!("second number is not allowed to be zero if dividing.", );
                }
                else if b'C' == parameters[0] && b'O' == parameters[1] && b'L' == parameters[2] && b'O' == parameters[3] && b'R' == parameters[4] {
                    println!("COMMAND: COLOR <color>");
                    println!();
                    println!("PARAMETERS:");
                    println!("Color: options are as follow.");
                    println!("Blue");
                    println!("Green");
                    println!("Cyan");
                    println!("Red");
                    println!("Magenta");
                    println!("LightGray");
                    println!("DarkGray");
                    println!("LightBlue");
                    println!("LightGreen");
                    println!("LightCyan");
                    println!("LightRed");
                    println!("Pink");
                    println!("Yellow");
                    println!("White");

                }
                else if b'C' == parameters[0] && b'O' == parameters[1] && b'P' == parameters[2] && b'Y' == parameters[3] {
                    println!("COMMAND: COPY <ROW> <COLUMN>");
                    println!();
                    println!("ROW:    counting from bottom up must be between 0 and 24");
                    println!("COLUMN: number of characters  to copy from left to right. between 0 and 80");
                }
                else {
                    println!("Unknow paramater passed. Run HELP for list of commands.");
                }
            }

        }

        else if b'M' == args[0] && b'A' == args[1] && b'T' == args[2] && b'H' == args[3] {
            if !params {
                println!("parameters required! run HELP MATH for more details")
            }
            else {
                use math;
                math::math(parameters)
            }

        }

        else if b'C' == args[0] && b'O' == args[1] && b'P' == args[2] && b'Y' == args[3] {
            if !params{
                println!("parameters required! run HELP COPY for more details");
            }
            else {
                use math;

                let mut first_num = 0;
                let mut second_num = 0;
                let mut next = false;

                for i in parameters.iter() {
                    if *i != 0{
                        if math::check_numb(*i) {
                            if !next {
                                first_num = first_num * 10 + (i - 48);
                            }
                            else {
                                second_num = second_num * 10 + (i - 48);
                            }
                        }
                        else if *i == b' '{
                            next = true;
                        }
                    }
                    else{
                        break;
                    }
                }

                if first_num < 24 && second_num < 80 {
                    use vga_buffer;
                    vga_buffer::command_copy(first_num as usize, second_num as usize);
                }
                else{
                    println!("parameters invalid! run HELP COPY for more details");

                }

            }
        }

    }

    else if command == 5 {

        // clear out the screen
        if b'C' == args[0] && b'L' == args[1] && b'E' == args[2] && b'A' == args[3] && b'R' == args[4] {

            for _i in 0..25 {
                println!();
            }
        }
        else if b'C' == args[0] && b'O' == args[1] && b'L' == args[2] && b'O' == args[3] && b'R' == args[4] {
            if !params {
                println!("paramaters are required");
            }
            else{
                use vga_buffer;

                if b'B' == parameters[0] && b'L' == parameters[1] && b'U' == parameters[2] && b'E' == parameters[3]{
                    vga_buffer::color_change(1);
                    println!("Blue!");
                }
                else if b'G' == parameters[0] && b'R' == parameters[1] && b'E' == parameters[2] && b'E' == parameters[3] && b'N' == parameters[4]{
                    vga_buffer::color_change(2);
                    println!("Green!");
                }
                else if b'C' == parameters[0] && b'Y' == parameters[1] && b'A' == parameters[2] && b'N' == parameters[3]{
                    vga_buffer::color_change(3);
                    println!("Cyan!");
                }
                else if b'R' == parameters[0] && b'E' == parameters[1] && b'D' == parameters[2]{
                    vga_buffer::color_change(4);
                    println!("Red!");
                }
                else if b'M' == parameters[0] && b'A' == parameters[1] && b'G' == parameters[2] && b'E' == parameters[3]
                        && b'N' == parameters[4] && b'T' == parameters[5] && b'A' == parameters[6]{
                    vga_buffer::color_change(5);
                    println!("Magenta!");
                }
                else if b'B' == parameters[0] && b'R' == parameters[1] && b'O' == parameters[2] && b'W' == parameters[3] && b'N' == parameters[4]{
                    vga_buffer::color_change(6);
                    println!("Brown!");
                }
                else if b'L' == parameters[0] && b'I' == parameters[1] && b'G' == parameters[2] && b'H' == parameters[3]
                        && b'T' == parameters[4] && b'G' == parameters[5] && b'R' == parameters[6] && b'A' == parameters[7] && b'Y' == parameters[8]{
                    vga_buffer::color_change(7);
                    println!("Light Gray!");
                }
                else if b'D' == parameters[0] && b'A' == parameters[1] && b'R' == parameters[2] && b'K' == parameters[3]
                        && b'G' == parameters[4] && b'R' == parameters[5] && b'A' == parameters[6] && b'Y' == parameters[7]{
                    vga_buffer::color_change(8);
                    println!("Dark Gray!");
                }
                else if b'L' == parameters[0] && b'I' == parameters[1] && b'G' == parameters[2] && b'H' == parameters[3]
                        && b'T' == parameters[4] && b'B' == parameters[5] && b'L' == parameters[6] && b'U' == parameters[7] && b'E' == parameters[8]{
                    vga_buffer::color_change(9);
                    println!("Light Blue!");
                }
                else if b'L' == parameters[0] && b'I' == parameters[1] && b'G' == parameters[2] && b'H' == parameters[3] && b'T' == parameters[4]
                        && b'G' == parameters[5] && b'R' == parameters[6] && b'E' == parameters[7] && b'E' == parameters[8] && b'N' == parameters[9]{
                    vga_buffer::color_change(10);
                    println!("Light Green!");
                }
                else if b'L' == parameters[0] && b'I' == parameters[1] && b'G' == parameters[2] && b'H' == parameters[3]
                        && b'T' == parameters[4] && b'C' == parameters[5] && b'Y' == parameters[6] && b'A' == parameters[7] && b'N' == parameters[8]{
                    vga_buffer::color_change(11);
                    println!("Light Cyan!");
                }
                else if b'L' == parameters[0] && b'I' == parameters[1] && b'G' == parameters[2] && b'H' == parameters[3]
                        && b'T' == parameters[4] && b'R' == parameters[5] && b'E' == parameters[6] && b'D' == parameters[7]{
                    vga_buffer::color_change(12);
                    println!("Light Red!");
                }
                else if b'P' == parameters[0] && b'I' == parameters[1] && b'N' == parameters[2] && b'K' == parameters[3]{
                    vga_buffer::color_change(13);
                    println!("Pink!");
                }
                else if b'Y' == parameters[0] && b'E' == parameters[1] && b'L' == parameters[2] && b'L' == parameters[3]
                        && b'O' == parameters[4] && b'W' == parameters[5] {
                    vga_buffer::color_change(14);
                    println!("Yellow!");
                }
                else if b'W' == parameters[0] && b'H' == parameters[1] && b'I' == parameters[2] && b'T' == parameters[3] && b'E' == parameters[4]{
                    vga_buffer::color_change(15);
                    println!("White!");
                }
                else{
                    println!("Invalid parameters. Please reference HELP COLOR");
                }
            }
        }
    }
    else if command == 6{
        if b'C' == args[0] && b'O' == args[1] && b'W' == args[2] && b'S' == args[3] && b'A' == args[4] && b'Y' == args[5] {
            print!(" ");
            for i in parameters.iter() {
                if *i != 0 {
                    print!("-");
                }
            }
            print!("--");
            println!(" ");
            print!("< ");

            for i in parameters.iter() {
                if *i != 0 {
                    print!("{}", str::from_utf8(&[*i]).unwrap());
                }
            }
            print!(" >");
            println!();
            print!(" ");
            for i in parameters.iter() {
                if *i != 0 {
                    print!("-");
                }
            }
            print!("--");
            println!();
            println!("        \\   ^__^");
            println!("         \\  (oo)\\_______");
            println!("            (__)\\       )\\/\\");
            println!("                ||----w |");
            println!("                ||     ||")
        }
    }

    else if command == 8 {

        // Call shutdown from prompt
        if b'S' == args[0] && b'H' == args[1] && b'U' == args[2] && b'T' == args[3] &&
            b'D' == args[4] && b'O' == args[5] && b'W' == args[6] && b'N' == args[7] {
            use power;
            unsafe {power::shutdown();};
        }
        if b'N' == args[0] && b'E' == args[1] && b'O' == args[2] && b'F' == args[3] && b'E' == args[4] && b'T' == args[5] && b'C' == args[6] && b'H' == args[7] {
            println!("___ _  _ _  _ ____ _  _    ____ ____       root@qemu-x86-virt");
            println!(" |  |  |  \\/  |___ |\\ |    |  | [__        --------------------------");
            println!(" |  |__| _/\\_ |___ | \\|    |__| ___]       OS: TuxenOS Pre-Alpha x86");
            println!("                                           Host: qemu-x86-virt");
            println!("              ___          ___             Kernel: TuxenOS DELUX v0.07");
            println!("             /\\  \\        /\\__\\            Uptime: true ");
            println!("      ___   /::\\  \\      /:/ _/_           Packages: 8");
            println!("     /\\__\\ /:/\\:\\  \\    /:/ /\\  \\          Shell: prmt.rs ");
            println!("    /:/  //:/  \\:\\  \\  /:/ /::\\  \\         Resolution: 80x24");
            println!("   /:/__//:/__/ \\:\\__\\/:/_/:/\\:\\__\\        DE: N/A ");
            println!("  /::\\  \\\\:\\  \\ /:/  /\\:\\/:/ /:/  /        WM: N/A");
            println!(" /:/\\:\\  \\\\:\\  /:/  /  \\::/ /:/  /         Theme: Hacker Theme ");
            println!(" \\/__\\:\\  \\\\:\\/:/  /    \\/_/:/  /          Icons: NULL ");
            println!("      \\:\\__\\\\::/  /       /:/  /           Terminal: yes");
            println!("       \\/__/ \\/__/        \\/__/            CPU: Exists");
            println!("                                           GPU: 0xb8000");
            println!("                                           Memory: None");

        }
    }
}
